import 'package:flutter/material.dart';
import 'package:rsywx/styles/styles.dart';
import 'package:url_launcher/url_launcher.dart';

GestureDetector _link(String text, String uri) {
  return GestureDetector(
    onTap: () => {launchUrl(Uri.parse(uri))},
    child: Text(
      text,
      style: linkStyle,
    ),
  );
}

class Heading extends StatelessWidget {
  const Heading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, top: 25),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _link('藏书', 'https://www.rsywx.net'),
                  const Text(
                    ' | ',
                  ),
                  _link('读书', 'https://rsywx.net/reading'),
                  const Text(
                    ' | ',
                  ),
                  _link('博客', 'https://blog.rsywx.net'),
                  const Text(
                    ' | ',
                  ),
                  _link('维客', 'http://rsywx.com'),
                  const Text(
                    ' | ',
                  ),
                  _link('资源', 'http://rsywx.com'),
                ],
              ),
              const Text(
                '任氏有无轩',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
