import 'package:flutter/material.dart';
import 'package:rsywx/provider/provider.dart';
import 'package:rsywx/model/book.dart';
import 'package:cached_network_image/cached_network_image.dart';

class RandomBookTabView extends StatefulWidget {
  const RandomBookTabView({Key? key}) : super(key: key);

  @override
  State<RandomBookTabView> createState() => _RandomBookTabViewState();
}

class _RandomBookTabViewState extends State<RandomBookTabView> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Book>>(
      future: getRandomBooks(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          var books = snapshot.data;

          return Container(
            margin: const EdgeInsets.only(top: 15),
            height: 210,
            child: ListView.builder(
              padding: const EdgeInsets.only(right: 6),
              itemCount: books?.length,
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                var book = books![index];

                return BookCard(book: book);
              },
            ),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text('Error: ${snapshot.error}'),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(
              strokeWidth: 2.0,
            ),
          );
        }
      },
    );
  }
}

class BookCard extends StatelessWidget {
  const BookCard({
    Key? key,
    required this.book,
  }) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      padding: const EdgeInsets.all(5),
      height: 210,
      width: 150,
      child: bookImageHelper(book),
    );
  }
}

