import 'package:flutter/material.dart';
import 'package:rsywx/widget/home/random_book_tabview.dart';
import 'new_book_tabview.dart';

class BooksRecommendation extends StatelessWidget {
  final TabController tabController;
  const BooksRecommendation({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber.shade400,
      height: 280,
      margin: const EdgeInsets.only(top: 25),
      padding: const EdgeInsets.only(left: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TabBar(
            controller: tabController,
            labelPadding: const EdgeInsets.all(0),
            indicatorPadding: const EdgeInsets.all(0),
            isScrollable: true,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            labelStyle: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),
            unselectedLabelStyle: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
            tabs: [
              Tab(
                child: Container(
                  margin: const EdgeInsets.only(right: 23),
                  child: const Text('新近登录'),
                ),
              ),
              Tab(
                child: Container(
                  margin: const EdgeInsets.only(right: 23),
                  child: const Text('随机推荐'),
                ),
              ),
              Tab(
                child: Container(
                  margin: const EdgeInsets.only(right: 23),
                  child: const Text('冷门书籍'),
                ),
              ),
            ],
          ),
          BooksRecommendationTab(tabController: tabController)
        ],
      ),
    );
  }
}

class BooksRecommendationTab extends StatelessWidget {
  final TabController tabController;
  const BooksRecommendationTab({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TabBarView(
        controller: tabController,
        children: <Widget>[
          const NewBookTabView(),
          const RandomBookTabView(),
          Container(
            child: const Text('Cold'),
          ),
        ],
      ),
    );
  }
}
