import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rsywx/model/book.dart';

import 'package:http/http.dart' as http;
import 'package:rsywx/provider/api_uri.dart';

const httpOk = 200;

Future<Map<String, dynamic>> getRead() async {
  const uri = 'https://api.rsywx.com/read';
  final response = await http.get(Uri.parse(uri));

  if (response.statusCode == httpOk) {
    var res = jsonDecode(response.body);
    //var hc = res.data;
    return res['data'];
  } else {
    throw Exception('Failed to load Reading summary data.');
  }
}

Future<List<Book>> getNewBooks() async {
  const uri = newBooksUri;
  final response = await http.get(Uri.parse(uri));

  if (response.statusCode == httpOk) {
    var res = jsonDecode(response.body);
    return (res['data'] as List).map((b) => Book.fromJson(b)).toList();
  } else {
    throw Exception('Failed to load New Books.');
  }
}

Future<List<Book>> getRandomBooks() async {
  const uri = randomBooksUri;
  final response = await http.get(Uri.parse(uri));

  if (response.statusCode == httpOk) {
    var res = jsonDecode(response.body);
    return (res['data'] as List).map((b) => Book.fromJson(b)).toList();
  } else {
    throw Exception('Failed to load Random Books.');
  }
}

Widget bookImageHelper(Book book) {
  var uri =
      'https://api.rsywx.com/book/image/${book.bookid}/${book.title}/${book.author}/150';

  return CachedNetworkImage(
    imageUrl: uri,
    progressIndicatorBuilder: (context, uri, progress) => Center(
        child: CircularProgressIndicator(
      value: progress.progress,
    )),
  );
}
