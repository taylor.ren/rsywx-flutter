import 'package:rsywx/model/review.dart';

class Book {
  final String id;
  final String bookid;
  final String title;
  final String author;
  final DateTime purchDate;
  final String intro;
  final List<Review> reviews;

  Book(this.id, this.bookid, this.title, this.author, this.purchDate,
      this.intro, this.reviews);

  Book.fromJson(Map<String, dynamic> json)
      : id = json['id'].toString(),
        bookid = json['bookid'],
        title = json['title'],
        author = json['author'],
        purchDate = DateTime.parse(json['purchdate']),
        intro = json['intro'],
        reviews = json['reviews'] == null
            ? []
            : (json['reviews'] as List).map((r) => Review.fromJson(r)).toList();
}
