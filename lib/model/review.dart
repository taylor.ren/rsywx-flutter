class Review {
  final int id;
  final String title;
  final DateTime datein;
  final String uri;

  Review(this.id, this.title, this.datein, this.uri);
  Review.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        datein = DateTime.parse(json['datein']),
        uri = json['URI'];
}
