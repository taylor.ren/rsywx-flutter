import 'dart:convert';

import 'package:test/test.dart';
import 'package:rsywx/model/review.dart';
import 'package:http/http.dart' as http;

void main() {
  test('Create a review from JSON', () async {
    var uri = 'https://api.rsywx.com/read/latest/1';
    var res = await http.get(Uri.parse(uri));

    var result = jsonDecode(res.body)['data'][0];

    var review = Review.fromJson(result);
    expect(review.id, 184);
    expect(review.title.startsWith('Theory of'), true);
    expect(review.datein, DateTime.parse('2022-01-30'));
    expect(review.uri.contains('theory-of-everything'), true);
  });
}
