import 'dart:convert';

import 'package:test/test.dart';
import 'package:rsywx/model/book.dart';
import 'package:http/http.dart' as http;

void main() {
  test('Retrieve a book with review(s)', () async {
    var uri = 'https://api.rsywx.com/book/00666';
    var res = await http.get(Uri.parse(uri));

    var result = jsonDecode(res.body)['data'];
    var book = Book.fromJson(result);

    expect(book.id, '666');
    expect(book.author, '卡尔维诺');
    expect(book.reviews.length, 2);
    expect(book.reviews[0].uri.contains('https://blog.rsywx.net'), true);
  });

  test('Retrieve a book with no review', () async {
    var uri = 'https://api.rsywx.com/book/00001';
    var res = await http.get(Uri.parse(uri));

    var result = jsonDecode(res.body)['data'];
    var book = Book.fromJson(result);

    expect(book.id, '1');
    expect(book.author, '佚名');
    expect(book.reviews.length, 0);
    //expect(book.reviews[0].uri.contains('https://blog.rsywx.net'), true);
  });

  test('Retrieve latest 9 books from API', () async {
    var uri = 'https://api.rsywx.com/book/latest/9';
    var res = await http.get(Uri.parse(uri));

    var result = jsonDecode(res.body)['data'];

    expect(result is List, true);
    var books = (result as List).map((b) => Book.fromJson(b)).toList();
    expect(books.length, 9);
    var book0 = books[0];
    expect(book0.id, '1992');
    expect(book0.title, '牛顿传');
  });
}
